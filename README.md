# Interictal Spike Detection

## Description
This repository can be used to detect interictal spikes in EEG recordings.
The data is first cleaned of artefacts.
Then spikes in raw signal are detected with `scipy.signal.find_peaks`.
Finally the spikes are classified between *real interictal spikes* and *false positive*
using a pre-trained classifier.

## Content
```
interictal_spike_detection
├── src
│   ├── find_interictal_spikes.py      # to detect interictal spikes
│   ├── train_spike_classifier.py      # to train a new classifier
│   ├── experiments_classif_spikes.py
│   └── extract_features.py
├── models
│   └── random_forest_classifier.pkl   # pre-trained spike classifier
├── LICENCE
├── requirements.txt                   # dependencies
└── README.md

```

The spike detection is done by `find_interictal_spikes.py`.
To get some help please call:
```
python src/find_interictal_spikes.py -h
```

A pre-traine model is present at `models/random_forest_classifier.pkl`.

If you want to train your own classifier, please use `train_spike_classifier.py`.
Be sure to get balanced classes (*true interictal spike* and *false positive*)
by fine tuning the `find_peaks_args` parameters.

## Installation
To install the dependencies, run :
```
pip install -r requirements.txt
```
Or for a local installation :
```
pip install --user -r requirements.txt
```
