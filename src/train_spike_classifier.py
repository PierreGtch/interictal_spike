description = '''
Train a classifier to detect true interictal spikes
'''
import os
import argparse
import time
import numpy as np
import pickle
import mne
from scipy.signal import find_peaks

from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, cross_validate, cross_val_predict, StratifiedKFold
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, precision_score, recall_score, make_scorer

from tabulate import tabulate

from artefacts import find_artefacts
from artefacts.utils_spectrum import integrated_psd
from artefacts.utils_mne import raw_to_epochs
from artefacts.utils_numpy import intervals_to_mask

from extract_features import extract_features  #relative import

#############################################################################
### parser :
# set default paths :
datadir = os.path.expanduser('~/data/EEGData')
model_path = os.path.join('/'.join(__file__.split('/')[:-2]+['models']), 'new_model.pkl')
basename = os.path.join(datadir, '20190503_14h41_3-Ch1-1a-')
raw_path = basename+       'raw.fif'
# epo_path = basename+'sorted-epo.fif'

# functions to automatically add arguments to the parser :
def add_dict_group(parser, title, dictionary, description=None, prefix=''):
    group = parser.add_argument_group(title, description=description)
    for k,v in dictionary.items():
        if type(v) not in [tuple, list]:
            group.add_argument('--'+prefix+k, type=type(v), help=' ', default=v)
        else:
            group.add_argument('--'+prefix+k, type=type(v[0]), nargs=(2 if len(v)==2 else '+'), help=' ', default=v)
    return group
def get_dict_group(args, dictionary, prefix=''):
    for k,v in list(dictionary.items()):
        dictionary[k] = getattr(args, prefix+k)

# argument parser :
parser = argparse.ArgumentParser(
    description=description,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-r', '--raw', type=str, nargs='+', default=[raw_path,], help='path to one or multiple mne.io.Raw object(s) (.fif format)')
parser.add_argument('-e', '--epo', type=str, nargs='+', help='OPTIONAL path to the corresponding mne.Epochs object(s) containing "good" interictal spikes')
parser.add_argument('-m', '--model', type=str, default=model_path, help='path to the output file where model will be saved (pickle format)')
parser.add_argument('-o', '--overwrite', action='store_true', default=False, help='overwrite output if existing')
parser.add_argument('--tmin', type=float, default=-.1, help='time (in seconds) before spike, only used if --epo is not specified')
parser.add_argument('--tmax', type=float, default= .4, help='time (in seconds) after  spike, only used if --epo is not specified')
find_artefacts_args = dict(
    t_window=5,
    max_ratio=2.,
    artefact_radius=5.,
    min_length=60.,
    online=True,
    preheat=20.,
    stride=.2
)
find_peaks_args = dict(height=(6e-4,3e-3), distance=6000, prominence=.5e-3, width=(5,300), wlen=1000, rel_height=0.5)
extract_features_args = dict(
    use_spectrum=True,
    use_spike_height=False,
    use_signal_mean=False,

    bands=(0., 3.5, 13., 128.),
    width = .15, #s
    stride= .02, #s
    # fmin = 0., #Hz
    # fmax = 125., #Hz
    # n_bands = 3,
    # spacing='lin',
)
add_dict_group(parser, 'find_artefacts',     find_artefacts_args, 'arguments of artefacts.find_artefacts()', prefix= 'art_')
add_dict_group(parser, 'find_peaks',             find_peaks_args, 'arguments of scipy.signal.find_peaks()' , prefix='peak_')
add_dict_group(parser, 'extract_features', extract_features_args, 'arguments of extract_features()',         prefix='feat_')
main_args = parser.parse_args()
get_dict_group(main_args,   find_artefacts_args,  'art_')
get_dict_group(main_args,       find_peaks_args, 'peak_')
get_dict_group(main_args, extract_features_args, 'feat_')
raw_paths        = main_args.raw
epo_paths        = main_args.epo
model_path       = main_args.model
OVERWRITE_OUTPUT = main_args.overwrite
if not OVERWRITE_OUTPUT and os.path.exists(model_path):
    raise ValueError(f'model_path {model_path} already exists')
assert epo_paths is None or len(raw_paths)==len(epo_paths), 'number of raw files different from number of epochs files'
# get signal parameters :
raw = mne.io.read_raw_fif(raw_paths[0], preload=False) # raw signal
sfreq = raw.info['sfreq']
if epo_paths is not None:
    sepochs = mne.read_epochs(epo_paths[0], preload=False) # true interictal spikes
    tmin = sepochs.tmin
    tmax = sepochs.tmax
else:
    tmin = main_args.tmin
    tmax = main_args.tmax



#############################################################################
### get data :

oepochs_list = []
labels_list  = []
for idx_file, raw_path in enumerate(raw_paths):
    print(f'-----\n\n\n {idx_file+1}/{len(raw_paths)} | {raw_path}')
    epo_path = None if epo_paths is None else epo_paths[idx_file]

    # load data :
    print(f'\n - loading data from {raw_path}')
    raw = mne.io.read_raw_fif(raw_path, preload=True) # raw signal
    assert len(raw.ch_names)==1, 'only implemented for one-channel signal'
    raw.filter(l_freq=.1, h_freq=None)
    raw_data = raw.get_data()
    if epo_path is not None:
        sepochs = mne.read_epochs(epo_path, preload=False) # true interictal spikes
        assert len(sepochs.ch_names)==1, 'only implemented for one-channel signal'
        assert tmin==sepochs.tmin and tmax==sepochs.tmax, 'inconsistant epoching between files'
        assert sfreq==sepochs.info['sfreq'],              'inconsistant sfreq between files'
    assert sfreq==raw.info['sfreq'], 'inconsistant sfreq between files'

    # find peaks and get epochs :
    # !! adjust the find_peaks parameters to get balanced classes (true interictal spike or not)
    print(f'\n - find_peaks')
    spikes_loc,_ = find_peaks(raw_data[0], **find_peaks_args)
    message_assert = f'''
    Some spikes present in "{epo_path}" were not found by scipy.signal.find_peaks.
    Please edit find_peaks arguments to be more permissive or check if the events in the epoch file are realy centered on spikes
    '''
    if epo_path is not None:
        assert np.all([i in spikes_loc for i in sepochs.events[:,0]]), message_assert

    # find artefacts :
    print(f'\n - find_artefacts')
    intervals, artefacts_mask = find_artefacts(raw_data, sfreq=sfreq,**find_artefacts_args)
    spikes_loc = spikes_loc[~artefacts_mask[spikes_loc]] # remove spikes detected in artefacts

    print(f'\n - load one epooch per spike')
    annotations = mne.Annotations(spikes_loc/sfreq, np.zeros(len(spikes_loc)), ['peak']*len(spikes_loc))
    raw.set_annotations(annotations)
    events, event_id = mne.events_from_annotations(raw)
    oepochs = mne.Epochs(raw, events, event_id, tmin, tmax, preload=True)
    oepochs.drop_bad()
    if epo_path is None:
        sepochs = oepochs.copy()
        print('\n - sorting events\nan mne plot window will open; please click on the undesired events then close the plot window')
        sepochs.plot(n_epochs=3, scalings='auto', block=True)
    ## labels : False if epoch contain a good spike, True otherwise
    labels = ~np.array([i in sepochs.events[:,0] for i in spikes_loc])

    sepochs  = None # free memory
    raw_data = None # free memory
    raw      = None # free memory
    oepochs_list.append(oepochs)
    labels_list .append(labels)
    print(f'num example: {len(labels)}, percent of positive: {np.mean(~labels)}')


# concatenale traning examples from all files :
labels  =  np.concatenate(labels_list, axis=0)
oepochs = mne.concatenate_epochs(oepochs_list)
print(f'\nTOTAL num example: {len(labels)}, percent of positive: {np.mean(~labels)}')


#############################################################################
# extract features :
print(f'\n - extracting features')
X = extract_features(oepochs.get_data().squeeze(axis=1), sfreq=sfreq,**extract_features_args)
print(f'num features : {X.shape[1]}, means : {X.mean(axis=0)}')



#############################################################################
# comparasion of models : (FACULTATIVE)
n_folds = 10

clfs = {
    "LogisticRegression": LogisticRegression(),
    "SVC_linear": SVC(kernel='linear'),
    'SVC_rbf':    SVC(kernel='rbf'),
    'SVC_poly':   SVC(kernel='poly'),
    'DecisionTreeClassifier': DecisionTreeClassifier(random_state=12),
    'RandomForestClassifier': RandomForestClassifier(random_state=12)
}

# metrics :
class Metric:
    def __init__(self, fun, **kwargs):
        self.fun = fun
        self.kwargs = kwargs
    def __call__(self, y_true, y_pred):
        return self.fun(y_true, y_pred, **self.kwargs)
# metrics = ['accuracy', 'precision', 'recall', 'f1', ]#'confusion_matrix']
# metrics_fun = [accuracy_score, precision_score, recall_score, f1_score]
metrics_fun = {
    'acc': accuracy_score,
    'P+' : Metric(precision_score, pos_label=0),
    'R+' : Metric(recall_score   , pos_label=0),
    'F1+': Metric(f1_score       , pos_label=0),
    'P-' : Metric(precision_score, pos_label=1),
    'R-' : Metric(recall_score   , pos_label=1),
    'F1-': Metric(f1_score       , pos_label=1),
}
scorers_fun = {k:make_scorer(f) for k,f in metrics_fun.items()}

print(f'n_folds = {n_folds}\n')
scores = {}
preds = {}
for name,clf in clfs.items():
    cv = StratifiedKFold(n_splits=n_folds, random_state=12, shuffle=True)
    # scores[name] = cross_validate(   clf, X, labels_train, cv=cv, scoring=scorers_fun)
    pred = cross_val_predict(clf, X, labels, cv=cv)
    preds[ name] = pred
    scores[name] = {k:f(labels, pred) for k,f in metrics_fun.items()}



#############################################################################
# print results : (FACULTATIVE)

names = preds.keys()
metrics_names = sorted(metrics_fun.keys(), key=lambda s:s[::-1])
headers = ['Classifiers']+metrics_names
tab = []
first = True
for n in names:
    tab.append([n])
    for m in metrics_names:
        tab[-1].append(scores[n][m])
print(tabulate(tab, headers=headers))



#############################################################################
# plot mne : (FACULTATIVE)
## the final classifier that will be saved :
# clf_name = 'DecisionTreeClassifier'
clf_name = 'RandomForestClassifier'
'''
  | p r e d
-----------
t | 0 | 1 |
r ---------
u | 2 | 3 |
e ---------
'''
conf_label_dict = dict(tp=(0,'g'), fn=(1,'r'), fp=(2,'y'), tn=(3,'b'))

conf_labels = 2*labels.astype(np.int32) + preds[clf_name].astype(np.int32)
unique, counts = np.unique(conf_labels, return_counts=True)
counts = dict(zip(unique, counts))
tab = [(k, counts.get(v,0), c) for k,(v,c) in conf_label_dict.items()]
tab = tabulate(tab, headers=['class', 'count', 'plot color'])
print(f'\nPrinting result details of clf "{clf_name}":\n{tab}\n')

c_dict = dict(conf_label_dict.values())
epoch_colors = [[c_dict[i],] for i in conf_labels]
oepochs.plot(scalings='auto', n_epochs=3, epoch_colors=epoch_colors, block=True)



#############################################################################
# train and save the final classifier :
clf = RandomForestClassifier(random_state=12)
clf.fit(X, labels)
out_dict = dict(
    find_peaks_args=find_peaks_args,
    find_artefacts_args=find_artefacts_args,
    extract_features_args=extract_features_args,
    clf=clf,
    tmin=tmin,
    tmax=tmax,
    sfreq=sfreq,
    raw_file=raw_paths,
    spike_reference_file=epo_paths,
)
with open(model_path, 'wb') as f:
    pickle.dump(out_dict, f)
print(f'model saved at {model_path}')
