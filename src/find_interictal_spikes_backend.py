description = '''
Detect spikes in raw signal with scipy.signal.find_peaks then classify interictal spikes with a pre-trained model
'''
import pickle
import numpy as np
import mne
from scipy.signal import find_peaks

from tabulate import tabulate

from artefacts import find_artefacts

from extract_features import extract_features  #relative import


def main(raw_path, model_path):
    '''
    please run `python find_interictal_spikes.py -h` to get informations about arguments

    Returns:
        oepochs: mne.Epochs object
            "original epochs" contains the events found by scipy.signal.find_peaks that are not artefacts
        sepochs: mne.Epochs object
            "sorted epochs" contains the events classified as "positive" by the classifier
        unfiltered_spikes_loc: array, shape (n_spikes,)
            output of the function scipy.signal.find_peaks()
        (intervals, artefacts_mask): pair of arrays
            output of the function artefacts.find_artefacts()
        epoch_colors: list of strings
            colors of the oepochs if you want to plot the classification result
    '''
    
    #############################################################################
    ### get data :
    # load spike classifier :
    print(f'\n - loading spike classifier from {model_path}')
    with open(model_path, 'rb') as f:
        model = pickle.load(f)
    find_peaks_args       = model['find_peaks_args']
    find_artefacts_args   = model['find_artefacts_args']
    extract_features_args = model['extract_features_args']
    tmin = model['tmin']
    tmax = model['tmax']
    clf  = model['clf']

    # load raw :
    print(f'\n - loading data from {raw_path}')
    raw = mne.io.read_raw_fif(raw_path, preload=True) # raw signal
    assert len(raw.ch_names)==1, 'only implemented for one-channel signal'
    raw.filter(l_freq=.1, h_freq=None)
    raw_data = raw.get_data()
    sfreq = raw.info['sfreq']
    assert sfreq==model['sfreq'], 'model was trained with a signal at a different sampling rate'



    #############################################################################
    ### get classifier's input :
    # find peaks :
    print(f'\n - find_peaks')
    unfiltered_spikes_loc,_ = find_peaks(raw_data[0], **find_peaks_args)

    # find artefacts :
    print(f'\n - find_artefacts')
    intervals, artefacts_mask = find_artefacts(raw_data, sfreq=sfreq, **find_artefacts_args)
    spikes_loc = unfiltered_spikes_loc[~artefacts_mask[unfiltered_spikes_loc]] # remove spikes detected in artefacts
    print(f'{len(spikes_loc)} spikes found')

    print(f'\n - load one epooch per spike')
    annotations = mne.Annotations(spikes_loc/sfreq, np.zeros(len(spikes_loc)), ['peak']*len(spikes_loc))
    raw.set_annotations(annotations)
    events, event_id = mne.events_from_annotations(raw)
    oepochs = mne.Epochs(raw, events, event_id, tmin, tmax)
    oepochs.drop_bad()



    #############################################################################
    # extract features :
    print(f'\n - extracting features')
    X = extract_features(oepochs.get_data().squeeze(axis=1), sfreq=sfreq, **extract_features_args)



    #############################################################################
    # make prediction :
    pred = clf.predict(X)
    pred = pred.astype(np.int)

    label_dict = dict(positive=(0,'g'), negative=(1,'b'))
    event_id = {k:v0 for k,(v0,_) in label_dict.items()}
    oepochs.events[:,2] = pred
    oepochs.event_id.update(event_id)

    sepochs = oepochs[~ pred.astype(np.bool)] # the sorted epochs

    #############################################################################
    # print result :
    unique, counts = np.unique(pred, return_counts=True)
    counts = dict(zip(unique, counts))
    tab = [(k, counts.get(v,0), c) for k,(v,c) in label_dict.items()]
    tab = tabulate(tab, headers=['class', 'count', 'plot color'])
    print(f'\nPrinting classification results:\n{tab}\n')


    c_dict = dict(label_dict.values())
    epoch_colors = [[c_dict[i],] for i in pred]

    return oepochs, sepochs, unfiltered_spikes_loc, (intervals, artefacts_mask), epoch_colors
