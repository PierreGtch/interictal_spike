'''
training of a classifier to separate real and wrong spikes
'''
import os
import time
import numpy as np
import matplotlib.pyplot as plt
import mne
from scipy.signal import find_peaks

from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, cross_validate, cross_val_predict, StratifiedKFold
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, precision_score, recall_score, make_scorer

from tabulate import tabulate

from artefacts.utils_spectrum import integrated_psd
from artefacts.utils_mne import raw_to_epochs
from artefacts.utils_numpy import intervals_to_mask

from extract_features import extract_features  #relative import

REMOVE_ARTEFACTS = True
ADVERSARIAL_EXAMPLES = False

good_intervals = np.array([## parts of 20190503_14h41_3-Ch1-1a without artefact
    [0,145187],[817304,1141812],[1171500,1857851],[2043812,2560726],[2693726,8958281],[9164890,9714812],[10034585,10205921],[10233156,12140148],[12179007,12684546],[12878843,13975718],[14047328,17754367],[17963640,18098820],[18161265,18323468],[18356468,18736765],[18757007,19582375],[19673320,23733492],[23985210,24199000],[24334992,27250226],[27361632,27488179],[27574601,27711023],[28334250,28478507],[28750281,33126578],[33460750,33929140],[34023562,34916781],[35038046,35597515],[35869656,36973664],[37246796,37438476],[37524546,37666984],[37722789,37940726],[38488328,38662476],[39003000,39225312],[39258007,39406046],[39657921,39971382],[40181796,40820429],[41867343,42775703],[42976484,43134867],[43213640,43869437],[44328773,44723929],[45035234,45812851],[46106140,46299609],[46390851,46939875],[47783148,51332078],[51399070,51519273],[51644554,51911742],[51983781,52638570],[52824039,53396046],[53618304,54454015],[54706796,54827140],[55046804,55608406],[55759007,56090265],[56143546,56398148],[56569882,56746273],[57091992,57493968],[57581765,63308671],[63338187,64161843],[64539531,65063085],[65315296,65927562],[66187023,66761843],[66785898,66986164],[67148148,67374929],[67475343,67781875],[67891757,68253234],[68494257,68786484],[69362632,69547117],[70009773,71047968],[71356757,71549335],[71633867,72288453],[72388578,72920414],[73003203,73732898],[73877882,73999226],[74077656,74398960],[74452976,74805125],[74837375,75085523],[75140617,83020070],[83234320,84449203],[84699656,85291000],[85601625,85725914],[86011679,86990421],[89404453,89563257],[90554882,91063117],[91119617,91845406],[92865593,93506367],[93813226,94071695],[94107484,94309601],[94735937,94864984],[95029843,95591367],[95681289,97614796],[97984398,98372476],[98417687,98605695],[98724906,98995570],[99069398,99348265],[99491703,100209695],[100233226,100354007],[100630687,100876953],[100993875,101309187],[101340335,101504921],[101547601,102216484],[102422476,107281617],[107313070,107478218],[107994656,108786234],[109073007,109527312],[109696984,110356492],[110838140,111569203],[111593750,116358164],[116525617,117309445],[117410281,125166078],[125216757,133989109],[134023312,134609390],[134642757,137194898],[137221140,141789617],[141864609,149161546],[149182812,151137750],[151167250,167573484],[167659187,167899398],[167947289,168083257],[168187101,172799992]
])


#############################################################################
### get data :
# set directories for saving results
datadir = os.path.expanduser('~/data/EEGData')
savedir = os.path.basename(__file__).split('.')[0] + '_results'
basename = os.path.join(datadir, '20190503_14h41_3-Ch1-1a-')

# load data :
raw = mne.io.read_raw_fif(basename+   'hpass-raw.fif', preload=False)
sepochs = mne.read_epochs(basename+  'sorted-epo.fif', preload=False)
# oepochs = mne.read_epochs(basename+'original-epo.fif', preload=False)
# spikes_loc = oepochs.events[:,0]
sfreq = sepochs.info['sfreq']

## find peaks and get epochs
# spikes_loc,_ = find_peaks(X, height=(1e-3,3e-3), threshold=None, distance=6000, prominence=.5e-3, width=(10,100), wlen=1000, rel_height=0.5, )
spikes_loc,_ = find_peaks(raw.get_data()[0], height=(6e-4,3e-3), threshold=None, distance=6000, prominence=.5e-3, width=(5,300), wlen=1000, rel_height=0.5, )
print(f'\n{len(spikes_loc)} spikes found')
assert np.all([i in spikes_loc for i in sepochs.events[:,0]])

# remove artefacts:
if REMOVE_ARTEFACTS:
    artefacts_mask = intervals_to_mask(good_intervals, len(raw))
    spikes_loc = spikes_loc[artefacts_mask[spikes_loc]]

annotations = mne.Annotations(spikes_loc/sfreq, np.zeros(len(spikes_loc)), ['peak']*len(spikes_loc))
raw.set_annotations(annotations)
events, event_id = mne.events_from_annotations(raw)
oepochs = mne.Epochs(raw, events, event_id, sepochs.tmin, sepochs.tmax)
oepochs.drop_bad()

## labels (False if epoch contain a good spike, True otherwise)
labels = ~np.array([i in sepochs.events[:,0] for i in spikes_loc])
signal = oepochs.get_data().squeeze(axis=1)


#############################################################################
## add negative epochs :
n_negative = 30
width = oepochs.tmax-oepochs.tmin
events = np.arange(1500, 4000, width) # interval without artefacts
annotations = mne.Annotations(events, np.zeros(len(events)), ['trial']*len(events))
raw.set_annotations(annotations)
events, event_id = mne.events_from_annotations(raw)
clean_epochs = mne.Epochs(raw, events, event_id, oepochs.tmin, oepochs.tmax)
clean_epochs.drop_bad()
# raw = raw[:, int(1500*sfreq):int(4000*sfreq)] # interval without artefacts
# clean_epochs = raw_to_epochs(raw, time_trials=width)
# clean_epochs = clean_epochs[1500//width:4000//width] # interval without artefacts
mask_epo = []
events = oepochs.events[:,0]
a0 = sfreq*(clean_epochs.tmin - oepochs.tmax)
a1 = sfreq*(clean_epochs.tmax - oepochs.tmin)
for idx_e,_,_ in clean_epochs.events:
    mask_epo.append(not np.any((events>idx_e+a0) & (events<idx_e+a1)))


clean_epochs = clean_epochs[mask_epo]
np.random.seed(12)
idx = np.random.choice(np.arange(len(clean_epochs)), n_negative, replace=False)
X = clean_epochs[idx].get_data().squeeze(1)
y = np.ones(X.shape[0], dtype=np.bool)
signal = np.concatenate([signal, X], axis=0)
labels = np.concatenate([labels, y], axis=0)


#############################################################################
### data augmentation ###
adv = np.empty((0,signal.shape[-1]))
bad_sig, good_sig = signal[labels], signal[~labels]
if ADVERSARIAL_EXAMPLES:
    # same spike height distribution :
    sph = signal.max(axis=1)
    adv2 = bad_sig / sph[labels,None] * np.random.choice(sph[~labels], bad_sig.shape[0])[:,None]
    adv = np.concatenate([adv, adv2], axis=0)
    # #same mean :
    # mean = np.abs(signal).mean(axis=1)
    # adv2 = bad_sig / mean[labels,None] * np.random.choice(mean[~labels], bad_sig.shape[0])[:,None]
    # adv = np.concatenate([adv, adv2], axis=0)
    # #same sq mean :
    # mean = (signal**2).mean(axis=1)
    # adv2 = bad_sig / mean[labels,None] * np.random.choice(mean[~labels], bad_sig.shape[0])[:,None]
    # adv = np.concatenate([adv, adv2], axis=0)

# concatenate original and augmented date :
percent_adv = .3
adv_idx = np.random.choice(np.arange(len(adv)), int(len(adv)*percent_adv),replace=False)
n_sig = len(signal)
signal = np.concatenate([signal, adv[adv_idx]], axis=0)
adv_label = np.ones(len(signal)-n_sig, dtype=np.bool)
# labels_train : False=good spike, True=bad spike
labels_train = np.concatenate([labels, adv_label], axis=0)
# labels_test : 0=good spike, 1=bad "authentic" spike, 2=bad adversarial spike
labels_test = np.concatenate([labels.astype(np.int32), adv_label.astype(np.int32)+1], axis=0)
print(f'num example: {len(labels_train)}, percent of positive: {np.mean(~labels_train)}')


#############################################################################
# extract features :
extract_features_args = dict(
    sfreq=sfreq,
    use_spectrum=True,
    use_spike_height=False,
    use_signal_mean=False,

    bands=(0., 3.5, 13., 128.),
    width = .15, #s
    stride= .02, #s
    # fmin = 0, #Hz
    # fmax = 125, #Hz
    # n_bands = 3,
    # spacing='lin',
)
X = extract_features(signal, **extract_features_args)
print(f'num features : {X.shape[1]}, means : {X.mean(axis=0)}')

#############################################################################
# learning
n_folds = 10

clfs = {
    "LogisticRegression": LogisticRegression(),
    "SVC_linear": SVC(kernel='linear'),
    'SVC_rbf':    SVC(kernel='rbf'),
    'SVC_poly':   SVC(kernel='poly'),
    'DecisionTreeClassifier': DecisionTreeClassifier(random_state=12),
    'RandomForestClassifier': RandomForestClassifier(random_state=12)#n_estimators=100, max_depth=2),
}

# metrics :
class Metric:
    def __init__(self, fun, **kwargs):
        self.fun = fun
        self.kwargs = kwargs
    def __call__(self, y_true, y_pred):
        return self.fun(y_true, y_pred, **self.kwargs)
metrics_fun = {
    'acc': accuracy_score,
    'P+' : Metric(precision_score, pos_label=0),
    'R+' : Metric(recall_score   , pos_label=0),
    'F1+': Metric(f1_score       , pos_label=0),
    'P-' : Metric(precision_score, pos_label=1),
    'R-' : Metric(recall_score   , pos_label=1),
    'F1-': Metric(f1_score       , pos_label=1),
}
scorers_fun = {k:make_scorer(f) for k,f in metrics_fun.items()}

print(f'n_folds = {n_folds}\n')
scores = {}
preds = {}
for name,clf in clfs.items():
    cv = StratifiedKFold(n_splits=n_folds, random_state=12, shuffle=True)
    pred = cross_val_predict(clf, X, labels_train, cv=cv)
    preds[ name] = pred
    scores[name] = {k:f(labels, pred) for k,f in metrics_fun.items()}


#############################################################################
# print results :

names = preds.keys()
metrics_names = sorted(metrics_fun.keys(), key=lambda s:s[::-1])
headers = ['Classifiers']+metrics_names
tab = []
first = True
for n in names:
    tab.append([n])
    for m in metrics_names:
        tab[-1].append(scores[n][m])
    # print confusion matrix :
    cmat = confusion_matrix(labels_test, preds[n].astype(np.int32), labels=[0,1,2])
    print(f'{n}:\n{cmat}\n')
print(tabulate(tab, headers=headers))



#############################################################################
# plot mne :
'''
  | p r e d
---------------
t | 0 | 1 | / |
r -------------
u | 2 | 3 | / |
e -------------
  | 4 | 5 | / |
---------------
'''
conf_label_dict = dict(tp=(0,'g'), fn=(1,'r'), fp=(2,'y'), tn=(3,'b'), adv_fp=(4,'m'), adv_tn=(5,'c'))

# clf_name = 'DecisionTreeClassifier'
clf_name = 'RandomForestClassifier'
conf_labels = 2*labels_test + preds[clf_name].astype(np.int32)
unique, counts = np.unique(conf_labels, return_counts=True)
counts = dict(zip(unique, counts))
tab = [(k, counts.get(v,0), c) for k,(v,c) in conf_label_dict.items()]
tab = tabulate(tab, headers=['class', 'count', 'plot color'])
print(f'\nPrinting result details of clf "{clf_name}":\n{tab}\n')

c_dict = dict(conf_label_dict.values())
epoch_colors = [[c_dict[i],] for i in conf_labels]
epo = mne.EpochsArray(signal[:,None,:], info=oepochs.info)
epo.plot(scalings='auto', n_epochs=3, epoch_colors=epoch_colors, block=True)
