import numpy as np
from artefacts.utils_spectrum import integrated_psd


def extract_features(
    signal, sfreq,
    use_spectrum=True,
    use_spike_height=False,
    use_signal_mean=False,
    width = .15, #s
    stride= .02, #s
    bands=None,
    fmin = 1.5, #Hz
    fmax = 125, #Hz
    n_bands = 5,
    spacing='log',
):
    '''
    Extracts features from a signal

    Args:
        signal: array, shape (n_trials, n_times)
            epoched signal
        sfreq: float
            sampling frequency
        use_spectrum: bool
            indicate whether the spectrum feature should be used (spectrum computed on windows of size 'width' )
        use_spike_height: bool
            indicate whether the spike height feature should be used (height of the highest sample in the trial)
        use_signal_mean: bool
            indicate whether the signal mean feature should be used (mean of the squared signal)
        width: float
            width (in seconds) of the windows on which the spectrums are computed
        stride: float
            stride (in seconds) between windows (for the spectrum feature)
        bands: list of floats (optional)
            list of frequencies between which spectrum is integrated (for the spectrum features)
            a list of length 4 means 3 intervals, so 3 scalar features per window
            If bands is None, fmin, fmax n_bands and spacing are used to compute the bands
        fmin: float
            min frequency (in Hz) of the bands (for the spectrum feature, if bands is None)
        fmax: float
            max frequency (in Hz) of the bands (for the spectrum feature, if bands is None)
        n_bands: int
            number of bands (for the spectrum feature, if bands is None)
        spacing: {'log' | 'lin'}
            distribution of the bands: linearly or logarithmically (for the spectrum feature, if bands is None)

    Returns:
        features: array, shape (n_trials, n_features)
            the feature matrix
    '''
    out = []

    if use_spectrum:
        if bands is not None:
            freq_bands = np.array(bands)
            assert freq_bands.ndim==1
        elif  spacing=='log':
            freq_bands = np.logspace(np.log(fmin), np.log(fmax), n_bands+1, base=np.e)
        elif spacing=='lin':
            freq_bands = np.linspace(fmin, fmax, n_bands+1)
        else:
            raise ValueError(f'invalid spacing {spacing}')
        freq_bands[0] = 0
        freq_bands = np.stack([freq_bands[:-1],freq_bands[1:]], axis=1)
        print("freq bands :\n", freq_bands)

        n_times = signal.shape[-1]
        i0, i1 = 0, int(sfreq*width)
        stride = int(sfreq*stride)
        while i1 <= n_times:
            bp = integrated_psd(signal[:,i0:i1], sfreq, freq_bands=freq_bands, axis=1)
            total_power = bp.sum(axis=1, keepdims=True) ## total bp
            out.append(bp / total_power)                ## relative bp
            out.append(total_power)
            i0 += stride
            i1 += stride
    if use_spike_height:
        spike_height = signal.max(axis=1, keepdims=True)
        out.append(spike_height)
    if use_signal_mean:
        mean_sq = (signal**2).mean(axis=1, keepdims=True)
        out.append(mean_sq)

    features = np.concatenate(out, axis=1)
    return features
