description = '''
Detect spikes in raw signal with scipy.signal.find_peaks then classify interictal spikes with a pre-trained model
'''
import os
import argparse
import time
import numpy as np
import pickle
import matplotlib.pyplot as plt
import mne
from scipy.signal import find_peaks

from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, cross_validate, cross_val_predict, StratifiedKFold
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score, precision_score, recall_score, make_scorer

from tabulate import tabulate

from artefacts import find_artefacts
from artefacts.utils_spectrum import integrated_psd
from artefacts.utils_mne import raw_to_epochs
from artefacts.utils_numpy import intervals_to_mask

# relative imports :
from extract_features import extract_features
from find_interictal_spikes_backend import main


#############################################################################
### parser :
# set default paths :
datadir = os.path.expanduser('~/data/EEGData')
basename = os.path.join(datadir, '20190503_14h41_3-Ch1-1a-')
raw_path = basename+       'raw.fif'
epo_path = basename+'sorted-epo.fif'
model_path = os.path.join('/'.join(__file__.split('/')[:-2]+['models']), 'random_forest_classifier.pkl')

# argument parser :
parser = argparse.ArgumentParser(
    description=description,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-r', '--raw', type=str, default=raw_path, help='path to an mne.io.Raw object (.fif format)')
parser.add_argument('-m', '--model', type=str, default=model_path, help='path to the trained classification model (pickle format)')
parser.add_argument('-d', '--dest', type=str, default=epo_path, help='path to the output file. i.e.  mne.Epochs object containing detected interictal spikes')
parser.add_argument('-o', '--overwrite', action='store_true', default=False, help='overwrite output if existing')
main_args = parser.parse_args()
raw_path         = main_args.raw
epo_path         = main_args.dest
model_path       = main_args.model
OVERWRITE_OUTPUT = main_args.overwrite
if not OVERWRITE_OUTPUT and os.path.exists(epo_path):
    raise ValueError(f'output path {epo_path} already exists')



#############################################################################
# find spikes :
oepochs, sepochs, unfiltered_spikes_loc, (intervals, artefacts_mask), epoch_colors = main(raw_path, model_path)



#############################################################################
# plot mne :
oepochs.copy().plot(scalings='auto', n_epochs=3, epoch_colors=epoch_colors, block=True)



#############################################################################
# save predictions :
sepochs.save(epo_path) # saving only the sorted epochs
